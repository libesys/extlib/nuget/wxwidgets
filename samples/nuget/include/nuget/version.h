/*!
 * \file nuget/version.h
 * \brief Version info for nuget
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

#ifndef __NUGET_VERSION_H__
#define __NUGET_VERSION_H__

// Bump-up with each new version
#define NUGET_MAJOR_VERSION    0
#define NUGET_MINOR_VERSION    0
#define NUGET_RELEASE_NUMBER   1
#define NUGET_VERSION_STRING   _T("nuget 0.0.1")

// Must be updated manually as well each time the version above changes
#define NUGET_VERSION_NUM_DOT_STRING   "0.0.1"
#define NUGET_VERSION_NUM_STRING       "0001"

// nothing should be updated below this line when updating the version

#define NUGET_VERSION_NUMBER (NUGET_MAJOR_VERSION * 1000) + (NUGET_MINOR_VERSION * 100) + NUGET_RELEASE_NUMBER
#define NUGET_BETA_NUMBER      1
#define NUGET_VERSION_FLOAT NUGET_MAJOR_VERSION + (NUGET_MINOR_VERSION/10.0) + (NUGET_RELEASE_NUMBER/100.0) + (NUGET_BETA_NUMBER/10000.0)

// check if the current version is at least major.minor.release
#define NUGET_CHECK_VERSION(major,minor,release) \
    (NUGET_MAJOR_VERSION > (major) || \
    (NUGET_MAJOR_VERSION == (major) && NUGET_MINOR_VERSION > (minor)) || \
    (NUGET_MAJOR_VERSION == (major) && NUGET_MINOR_VERSION == (minor) && NUGET_RELEASE_NUMBER >= (release)))

#endif

