/*!
 * \file nuget/nuget_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

// nuget.cpp : source file that includes just the standard includes
// nuget.pch will be the pre-compiled header
// nuget.obj will contain the pre-compiled type information

#include "nuget/nuget_prec.h"

// TODO: reference any additional headers you need in nuget_prec.h
// and not in this file

